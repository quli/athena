#include "../TrigROBSelector.h"
#include "../TrigROBListWriter.h"
#include "../TrigSubDetListWriter.h"
#include "../TrigEtaHypo.h"
#include "../TrigCheckForTracks.h"
#include "../TrigCheckForMuons.h"
#include "../ScoutingStreamWriter.h"
#include "../TrigL1CaloOverflow.h"

DECLARE_COMPONENT( TrigROBSelector )
DECLARE_COMPONENT( TrigROBListWriter )
DECLARE_COMPONENT( TrigSubDetListWriter )
DECLARE_COMPONENT( TrigEtaHypo )
DECLARE_COMPONENT( TrigCheckForTracks )
DECLARE_COMPONENT( TrigCheckForMuons )
DECLARE_COMPONENT( ScoutingStreamWriter )
DECLARE_COMPONENT( TrigL1CaloOverflow )
