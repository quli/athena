#
# File specifying the location of EvtGen to use.
#

set( EVTGEN_LCGVERSION 1.6.0 )
set( EVTGEN_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/evtgen/${EVTGEN_LCGVERSION}/${LCG_PLATFORM} )
