#
# File specifying the location of ThePEG to use.
#

set( THEPEG_LCGVERSION 2.1.5 )
set( THEPEG_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/thepeg/${THEPEG_LCGVERSION}/${LCG_PLATFORM} )
